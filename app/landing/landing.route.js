(function() {
    'use strict';

    angular
        .module('app.landing')
        .config(configFunction);

    configFunction.$inject = ['$stateProvider'];

    function configFunction($stateProvider) {
        $stateProvider.state('landing', {
            url: '/',
            controller: 'AuthCtrl as vm',
            templateUrl: 'app/landing/login.html'
        });
    }

})();
