(function() {
    'use strict';

    angular
        .module('app.auth')
        .config(configFunction)
//        .run(runFunction);

    configFunction.$inject = ['$stateProvider'];

    function configFunction($stateProvider) {

        $stateProvider.state('register', {
            url: '/register',
            controller: 'AuthCtrl as vm',
            templateUrl: 'app/auth/register.html'
        })

        $stateProvider.state('registertenant', {
            url: '/registertenant',
            controller: 'AuthCtrl as vm',
            templateUrl: 'app/auth/registertenant.html'
        })
    }

})();
