(function() {
    'use strict';

     angular
        .module('app.account')
        .controller('AccountCtrl', AccountCtrl)
        .controller('ProfileCtrl', ProfileCtrl)

        AccountCtrl.$inject = ['$state', '$scope', 'AuthService', 'AdminService', '$cookies', 'authData'];

        function AccountCtrl($state, $scope, AuthService, AdminService, $cookies, authData) {
            var vm = this;
            var mobileView = 992;

            AdminService.getUser(authData).$loaded().then(function(res) {
                vm.user = res;
                if (vm.user.type === "Admin") {
                    $scope.items = [
                        {link: ".dashboard", name: "Dashboard", icon: "menu-icon fa fa-tachometer"},
                        {link: ".customers", name: "Customers", icon: "menu-icon fa fa-heartbeat"},
                        {link: ".activities", name: "Activities", icon: "menu-icon fa fa-share-alt"},
                        {link: ".system", name: "System", icon: "menu-icon fa fa-gears"},
                        {link: ".tools", name: "Tools", icon: "menu-icon fa fa-wrench"}
                    ];
                }
                if (vm.user.type === "Sales") {
                    $scope.items = [
                        {link: ".dashboard", name: "Dashboard", icon: "menu-icon fa fa-tachometer"},
                        {link: ".customers", name: "Customers", icon: "menu-icon fa fa-heartbeat"},
                        {link: ".activities", name: "Activities", icon: "menu-icon fa fa-share-alt"}
                    ];
                }
            });

            $scope.getWidth = function() {
                return window.innerWidth;
            };

            $scope.$watch($scope.getWidth, function(newValue, oldValue) {
                if (newValue >= mobileView) {
                    if (angular.isDefined($cookies.get('toggle'))) {
                        $scope.toggle = ! $cookies.get('toggle') ? false : true;
                    } else {
                        $scope.toggle = true;
                    }
                } else {
                    $scope.toggle = false;
                }
            });

            window.onresize = function() {
                $scope.$apply();
            };

            $scope.toggleSidebar = function() {
                $scope.toggle = !$scope.toggle;
                $cookies.put('toggle', $scope.toggle);
            };

            $scope.logout = function() {
                AuthService.logout();
                $state.go('landing');
            };

        }

// controller('ProfileCtrl', ['Auth', 'AlertService', 'Messages', '$scope', '$state', '$cookieStore', 'profile',

        ProfileCtrl.$inject = ['$state', '$scope', 'authService', 'AdminService'];

        function ProfileCtrl($state, $scope, authService, AdminService) {
            var vm = this;
            vm.error = null;
            vm.profile = profile;

            vm.updateProfile = updateProfile;
            vm.forgotPassword = forgotPassword;
            vm.newPassword = newPassword;

//            function updateProfile() {
//                vm.profile.$save();
//                .catch(function(error) {
//                  vm.error = error;
//                });
//            };

            function forgotPassword() {
                authService.$resetPassword({ email: vm.profile.email })
                .then(function() {
                    AlertService.addSuccess(Messages.send_email_success);
                    $state.go('catalog.home');
                })
                .catch(function(error) {
                    console.error("Error: ", error);
                });
            };

            function newPassword() {
                if (vm.profile.new_password == vm.profile.confirm_new_password) {
                    authService.$changePassword({ email: vm.profile.email, oldPassword: vm.profile.password, newPassword: vm.profile.new_password })
                    .then(function() {
                        AlertService.addSuccess(Messages.save_password_success);
                        vm.profile.password = null;
                        vm.profile.new_password = null;
                        vm.profile.confirm_new_password = null;})
                    .catch(function(error) {
                        console.error("Error: ", error);
                    });
                } else {
                    AlertService.addError(Messages.passwords_dont_match);
                };
            };

        }

})();
