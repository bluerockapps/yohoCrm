(function() {
    'use strict';

     angular
        .module('app.activities')
        .controller('ActivitiesCtrl', ActivitiesCtrl)
        .controller('ActivityCtrl', ActivityCtrl)
        .controller('DiscoveryCtrl', DiscoveryCtrl)
        .controller('ProposalCtrl', ProposalCtrl)

        ActivitiesCtrl.$inject = ['$state', 'ActivitiesService'];

        function ActivitiesCtrl($state, ActivitiesService) {
            var vm = this;
            vm.activityTemplate = 'app/activities/gridTemplates/activityTemplate.html';

            ActivitiesService.activities().$loaded().then(function(res) {
                vm.gridActivities.data = res;
            });

            vm.editActivity = function(row) {
                $state.go('account.activity', {'rowEntity': row.entity});
            };

            vm.gridActivities = {
                showGridFooter: true,
                enableFiltering: true,
                columnDefs: [
                    { name: '', field: '$id', shown: false, cellTemplate: 'app/activities/gridTemplates/editActivity.html',
                        width: 34, enableColumnMenu: false, headerTooltip: 'Edit Activity', enableCellEdit: false, enableFiltering: false },
                    { name:'companyName', field: 'company_name', enableHiding: false, enableCellEdit: false},
                    { name:'contactName', field: 'contact_full_name', enableHiding: false, enableFiltering: false, enableSorting: true, enableCellEdit: false},
                    { name: 'activityStatus', field: 'info_status_label', cellTemplate: vm.activityTemplate, enableHiding: false, enableCellEdit: false,
                        enableFiltering: false, enableSorting: false, headerTooltip: 'Edit Activity'}]
            };

        }

        ActivityCtrl.$inject = ['$state', '$stateParams', '$timeout', 'ActivityService', 'CustomerSetupService', 'ContactService', 'CompanyService'];

        function ActivityCtrl($state, $stateParams, $timeout, ActivityService, CustomerSetupService, ContactService, CompanyService) {
            var vm = this;
            var obj = {};
            vm.activity = {};
            vm.company = {};
            vm.contact = {};
            vm.activity_contact = {};
            vm.company_count = null;
            vm.contact_id = null;
            vm.new_button = null;
            vm.next_button = null;
            vm.contact_label = 'Selected Contact';
            vm.activity.dt = new Date();
            vm.activity.date = vm.activity.dt.toDateString();
            vm.activity.time = vm.activity.dt.toLocaleTimeString();

            ActivityService.activityTypes().$loaded().then(function(res) {
                vm.activity_types = res;
            });

            ActivityService.contacts().$loaded().then(function(res) {
                vm.contacts = res;
            });

            CustomerSetupService.industryGroups().$loaded().then(function(res) {
                vm.groups = res;
            });

            CustomerSetupService.regions().$loaded().then(function(res) {
                vm.regions = res;
            });

            vm.loadActivity = function (id) {
                ActivityService.activity(id).$loaded().then(function(res) {
                    vm.activity = res;
                    if (vm.activity.info_status === true) {
                        vm.activity.dt = new Date(res.info_date_added);
                        vm.activity.date_added = vm.activity.dt.toDateString();
                        vm.activity.time_added = vm.activity.dt.toLocaleTimeString();
                        vm.activity.dt = new Date(res.info_date_updated);
                        vm.activity.date_updated = vm.activity.dt.toDateString();
                        vm.activity.time_updated = vm.activity.dt.toLocaleTimeString();
                        ContactService.contact(res.contact_id).$loaded().then(function(res) {
                            vm.contact = res;
                            vm.contact_id = res.$id;
                            vm.activity_contact.selected = {full_name: res.full_name};
                        });
                    }
                });
            };

            if ($stateParams.rowEntity != null) {
                vm.new_button = 'off';
                vm.activity_id = $stateParams.rowEntity.$id;
                vm.loadActivity($stateParams.rowEntity.$id);
            } else {
                vm.activity_id = null;
                vm.contact_id = null;
                vm.new_button = 'on';
            }

            vm.addActivity = function() {
                obj = {};
                obj.info_status = true;
                obj.current_status = 'info';
                obj.contact_id = vm.contact_id;
                obj.company_name = vm.contact.company_name;
                obj.contact_full_name = vm.contact.first_name + ' ' + vm.contact.last_name;
                obj.activity_type_id = vm.activity.activity_type_id;
                ActivityService.addActivity(obj).then(function(id) {
                    obj.$id = id;
                    obj.contact_id = vm.contact_id;
                    $state.go('account.activity.discovery', {'rowEntity': obj});
                });
            }, function(error) {
                vm.error = error;
            };

            vm.updateActivity = function() {
                obj = {};
                obj.id = vm.activity_id;
                obj.contact_id = vm.contact_id;
                obj.company_name = vm.contact.company_name;
                obj.contact_full_name = vm.contact.first_name + ' ' + vm.contact.last_name;
                obj.activity_type_id = vm.activity.activity_type_id;
                ActivityService.updateActivity(obj);
                $state.go('account.activity.discovery');
            }, function(error) {
                vm.error = error;
            };

            vm.getContact = function(obj) {
                vm.contact = angular.copy(obj);
                vm.contact_id = vm.contact.$id;
                if (vm.activity_id === null)
                    vm.new_button = 'on';
                vm.next_button = null;
                if (vm.contact.companies != undefined)
                    vm.company_count = Object.keys(vm.contact.companies).length;
                else
                    vm.company_count = null;
            }, function(error) {
                vm.error = error;
            };

            vm.updateContact = function() {
                if (vm.contact_id !== null && vm.contact_id !== 'new')
                    vm.contact.$save();
            }, function(error) {
                vm.error = error;
            };

            vm.newContact = function() {
                vm.contact_id = 'new';
                vm.contact = {};
                vm.new_button = 'off';
                vm.next_button = 'new';
                vm.contact_label = 'New Contact';
                vm.activity_contact.selected = null;
                vm.company_count = null;
            }, function(error) {
                vm.error = error;
            };

            vm.addContact = function() {
                vm.contact.view_status = true;
                vm.contact.full_name = vm.contact.first_name + ' ' + vm.contact.last_name;
                ContactService.addContact(vm.contact).then(function(id) {
                    ContactService.contact(id).$loaded().then(function(res) {
                        vm.contact = res;
                        vm.contact_id = res.$id;
                        vm.activity_contact.selected = {full_name: res.full_name};
                        vm.next_button = null;
                        vm.new_button = 'on';
                    });
                });
            }, function(error) {
                vm.error = error;
            };

        }


        DiscoveryCtrl.$inject = ['$state', '$scope', '$stateParams', '$timeout', 'ActivityService', 'CustomerSetupService', 'ActivitySetupService',
            'ContactService', 'ContactLoadPreferences', 'ContactLoadKpis'];

        function DiscoveryCtrl($state, $scope, $stateParams, $timeout, ActivityService, CustomerSetupService, ActivitySetupService,
            ContactService, ContactLoadPreferences, ContactLoadKpis) {
            var vm = this;
            var obj = {};
            var cnt = {};
            vm.activity = {};
            vm.discovery = {};
            vm.contact_preference = {};
            vm.contact_kpi = {};
            vm.discovery.dt = new Date();
            vm.discovery.date = vm.discovery.dt.toDateString();
            vm.discovery.time = vm.discovery.dt.toLocaleTimeString();

            $scope.preferences = {
                selected: null,
                lists: {"P": []}
            };

            $scope.kpis = {
                selected: null,
                lists: {"K": []}
            };

            ActivitySetupService.businessQuestions().$loaded().then(function(res) {
                vm.business_questions = res;
            });

            ActivitySetupService.solutionQuestions().$loaded().then(function(res) {
                vm.solution_questions = res;
            });

            vm.loadActivity = function (id) {
                ActivityService.activity(id).$loaded().then(function(res) {
                    vm.activity = res;
                    if (vm.activity.discovery_status === true) {
                        vm.discovery.dt = new Date(res.discovery_date_added);
                        vm.discovery.date_added = vm.discovery.dt.toDateString();
                        vm.discovery.time_added = vm.discovery.dt.toLocaleTimeString();
                        vm.discovery.dt = new Date(res.discovery_date_updated);
                        vm.discovery.date_updated = vm.discovery.dt.toDateString();
                        vm.discovery.time_updated = vm.discovery.dt.toLocaleTimeString();
                    }
                    vm.contact_id = res.contact_id;
                    ContactService.preferencesSnapShot().once('value').then(function(res) {
                        res.forEach(function(snapShotChild) {
                            obj = {};
                            obj.contact_id = res.$id;
                            obj.key = snapShotChild.getKey();
                            ContactService.contactPreference(obj).$loaded().then(function(res) {
                                if (res.$value === null) {
                                    obj.cnt = cnt;
                                    obj.key = res.$id;
                                    obj.notes = null;
                                    obj.label = snapShotChild.val().label;
                                    ContactService.contactSetPreference(obj);
                                    ContactService.contactSetPreferencePriority(obj);
                                    cnt = cnt + 1;
                                }
                            });
                        });
                        ContactService.contactPreferences(vm.contact_id).$loaded().then(function(res) {
                            vm.contact_preference.label = res[0].label;
                            vm.contact_preference.$id = res[0].$id;
                            if (res[0].notes !== undefined)
                                vm.contact_preference.notes = res[0].notes;
                            $scope.preferences.lists.P = res;
                        });
                    });
                    ContactService.kpisSnapShot().once('value').then(function(res) {
                        res.forEach(function(snapShotChild) {
                            obj = {};
                            obj.contact_id = res.$id;
                            obj.key = snapShotChild.getKey();
                            ContactService.contactKpi(obj).$loaded().then(function(res) {
                                if (res.$value === null) {
                                    obj.cnt = cnt;
                                    obj.key = res.$id;
                                    obj.notes = null;
                                    obj.label = snapShotChild.val().label;
                                    ContactService.contactSetKpi(obj);
                                    ContactService.contactSetKpiPriority(obj);
                                    cnt = cnt + 1;
                                }
                            });
                        });
                        ContactService.contactKpis(vm.contact_id).$loaded().then(function(res) {
                            vm.contact_kpi.label = res[0].label;
                            vm.contact_kpi.$id = res[0].$id;
                            if (res[0].notes !== undefined)
                                vm.contact_kpi.notes = res[0].notes;
                            $scope.kpis.lists.K = res;
                        });
                    });
                    ContactService.contact(vm.contact_id).$loaded().then(function(res) {
                        vm.contact = res;
                    });
                });
            };

            if (vm.activity.current_status === 'info') {
                vm.discovery_id = null;
                ContactService.contact(res.contact_id).$loaded().then(function(res) {
                    vm.contact = res;
                    vm.contact_id = res.$id;
                    if (vm.contact.companies != undefined)
                        vm.company_count = Object.keys(vm.contact.companies).length;
                });
            };

            if ($stateParams.rowEntity != null) {
                vm.activity_id = $stateParams.rowEntity.$id;
                vm.loadActivity($stateParams.rowEntity.$id);
            } else {
                vm.activity_id = null;
                vm.discovery_id = null;
            }

            vm.loadContactCompanies = function(id){
                vm.contact_companies = [];
                ContactService.contactCompanies(id).$loaded().then(function(res){
                    for(var i = 0; i < res.length; i++) {
                        var theRef = res[i];
                        CompanyService.company(theRef.$id).$loaded().then(function(theCompany){
                                theCompany.date_added = theRef.date_added;
                                vm.contact_companies.push(theCompany);
                        });
                    };
                });
            };

            vm.updateContact = function() {
                if (vm.contact_id !== null)
                    vm.contact.$save();
            }, function(error) {
                vm.error = error;
            };

            vm.updateDiscoveryDate = function() {
                obj.id = vm.activity_id;
                obj.current_status = vm.activity.current_status;
                ActivityService.updateDiscovery(obj);
            }, function(error) {
                vm.error = error;
            };

            vm.updateDiscovery = function() {
                obj.id = vm.activity_id;
                obj.current_status = vm.activity.current_status;
                ActivityService.updateDiscovery(obj);
                $state.go('account.activity.proposal');
            }, function(error) {
                vm.error = error;
            };

            vm.updatePreferenceNote = function(key, label) {
                obj = {};
                obj.key = key;
                obj.contact_id = vm.contact_id;
                obj.notes = vm.contact_preference.notes
                ContactService.contactUpdatePreferenceNote(obj);
                vm.updateDiscoveryDate();
            };

            vm.selectPreference = function() {
                obj = {};
                obj.contact_id = vm.contact_id;
                obj.key = $scope.preferences.selected.$id;
                ContactService.contactPreference(obj).$loaded().then(function(res) {
                    vm.contact_preference = res;
                });
                vm.updateDiscoveryDate();
            };

            vm.preferenceRanking = function($index, key, event) {
                obj = {};
                var cnt = 1;
                var data = $scope.preferences.lists.P;
                for(var i = 0; i < data.length; i++) {
                    obj.key = data[i].$id;
                    obj.cnt = cnt;
                    obj.contact_id = vm.contact_id;
                    ContactService.contactSetPreferencePriority(obj);
                    cnt = cnt + 1;
                }
                vm.updateDiscoveryDate();
            };

            vm.updateKpiNote = function(key, label) {
                obj = {};
                obj.key = key;
                obj.contact_id = vm.contact_id;
                obj.notes = vm.contact_kpi.notes
                ContactService.contactUpdateKpiNote(obj);
                vm.updateDiscoveryDate();
            };

            vm.selectKpi = function() {
                obj = {};
                obj.contact_id = vm.contact_id;
                obj.key = $scope.kpis.selected.$id;
                ContactService.contactKpi(obj).$loaded().then(function(res) {
                    vm.contact_kpi = res;
                });
                vm.updateDiscoveryDate();
            };

            vm.kpiRanking = function($index, key, event) {
                obj = {};
                var cnt = 1;
                var data = $scope.kpis.lists.K;
                for(var i = 0; i < data.length; i++) {
                    obj.key = data[i].$id;
                    obj.cnt = cnt;
                    obj.contact_id = vm.contact_id;
                    ContactService.contactSetKpiPriority(obj);
                    cnt = cnt + 1;
                }
                vm.updateDiscoveryDate();
            };

            vm.updateBusinessAnswer = function(name) {
                obj = {};
                obj.id = vm.contact_id;
                obj.name = name;
                var dt = new Date();
                obj.date = dt.toDateString();
                obj.time = dt.toLocaleTimeString();
                if (name === 'question1') obj.answer = vm.contact.business_answers.question1.answer;
                if (name === 'question2') obj.answer = vm.contact.business_answers.question2.answer;
                if (name === 'question3') obj.answer = vm.contact.business_answers.question3.answer;
                if (name === 'question4') obj.answer = vm.contact.business_answers.question4.answer;
                if (name === 'question5') obj.answer = vm.contact.business_answers.question5.answer;
                if (name === 'question6') obj.answer = vm.contact.business_answers.question6.answer;
                if (name === 'question7') obj.answer = vm.contact.business_answers.question7.answer;
                if (name === 'question8') obj.answer = vm.contact.business_answers.question8.answer;
                if (name === 'question9') obj.answer = vm.contact.business_answers.question9.answer;
                ActivityService.updateBusinessAnswer(obj);
                vm.updateDiscoveryDate();
            }, function(error) {
                vm.error = error;
            };

            vm.updateSolutionAnswer = function(name) {
                obj = {};
                obj.id = vm.contact_id;
                obj.name = name;
                var dt = new Date();
                obj.date = dt.toDateString();
                obj.time = dt.toLocaleTimeString();
                if (name === 'question1') obj.answer = vm.contact.solution_answers.question1.answer;
                if (name === 'question2') obj.answer = vm.contact.solution_answers.question2.answer;
                if (name === 'question3') obj.answer = vm.contact.solution_answers.question3.answer;
                if (name === 'question4') obj.answer = vm.contact.solution_answers.question4.answer;
                if (name === 'question5') obj.answer = vm.contact.solution_answers.question5.answer;
                if (name === 'question6') obj.answer = vm.contact.solution_answers.question6.answer;
                if (name === 'question7') obj.answer = vm.contact.solution_answers.question7.answer;
                if (name === 'question8') obj.answer = vm.contact.solution_answers.question8.answer;
                if (name === 'question9') obj.answer = vm.contact.solution_answers.question9.answer;
                ActivityService.updateSolutionAnswer(obj);
                vm.updateDiscoveryDate();
            }, function(error) {
                vm.error = error;
            };

        }


        ProposalCtrl.$inject = ['$state', '$stateParams', 'ActivityService', 'ContactService'];

        function ProposalCtrl($state, $stateParams, ActivityService, ContactService) {
            var vm = this;
            var obj = {};
            vm.activity = {};
            vm.proposal = {};
            vm.proposal.dt = new Date();
            vm.proposal.date = vm.proposal.dt.toDateString();
            vm.proposal.time = vm.proposal.dt.toLocaleTimeString();

            vm.loadActivity = function (id) {
                ActivityService.activity(id).$loaded().then(function(res) {
                    vm.activity = res;
                    if (vm.activity.proposal_status === true) {
                        vm.proposal.dt = new Date(res.discovery_date_added);
                        vm.proposal.date_added = vm.proposal.dt.toDateString();
                        vm.proposal.time_added = vm.proposal.dt.toLocaleTimeString();
                        vm.proposal.dt = new Date(res.discovery_date_updated);
                        vm.proposal.date_updated = vm.proposal.dt.toDateString();
                        vm.proposal.time_updated = vm.proposal.dt.toLocaleTimeString();
                    }
                    vm.contact_id = res.contact_id;
                    ContactService.contact(vm.contact_id).$loaded().then(function(res) {
                        vm.contact = res;
                    });
                    obj.contact_id = vm.contact_id;
                    obj.activity_id = vm.activity_id;
                    ActivityService.activityDocs(obj).$loaded().then(function(res) {
                        vm.docs = res;
                    });
                });
            };

            if ($stateParams.rowEntity != null) {
                vm.activity_id = $stateParams.rowEntity.$id;
                vm.loadActivity($stateParams.rowEntity.$id);
            } else {
                vm.activity_id = null;
                vm.proposal_id = null;
            }

            vm.updateProposal = function() {
                obj.id = vm.activity_id;
                obj.current_status = vm.activity.current_status;
                ActivityService.updateProposal(obj);
                $state.go('account.activity.quote');
            }, function(error) {
                vm.error = error;
            };

            var storageRef = firebase.storage().ref();

            vm.uploadFiles = function(files) {
                angular.forEach(files, function(file) {
                    obj = {};
                    var metadata = {
                        'contentType': file.type
                    };
                    if (metadata.contentType === 'image/jpeg') obj.icon = 'fa-file-image-o';
                    if (metadata.contentType === 'image/gif') obj.icon = 'fa-file-image-o';
                    if (metadata.contentType === 'image/png') obj.icon = 'fa-file-image-o';
                    if (metadata.contentType === 'video/quicktime') obj.icon = 'fa-file-video-o';
                    if (metadata.contentType === 'application/zip') obj.icon = 'fa-file-zip-o';
                    if (metadata.contentType === 'application/pdf') obj.icon = 'fa-file-pdf-o';
                    if (metadata.contentType === 'application/msword') obj.icon = 'fa-file-word-o';
                    if (metadata.contentType === 'application/excel') obj.icon = 'fa-file-excel-o';
                    if (metadata.contentType === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') obj.icon = 'fa-file-word-o';
                    if (metadata.contentType === 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') obj.icon = 'fa-file-excel-o';
                    var node = 'contacts/'+vm.contact_id+'/';
                    var uploadTask = storageRef.child(node + file.name).put(file, metadata);
                    uploadTask.on('state_changed', null, function(error) {
                        vm.error = error;
                    }, function() {
                        obj.contact_id = vm.contact_id;
                        obj.activity_id = vm.activity_id;
                        obj.view_status = true;
                        obj.total_bytes = uploadTask.snapshot.totalBytes;
                        obj.content_type = uploadTask.snapshot.metadata.contentType;
                        obj.name = uploadTask.snapshot.metadata.name;
                        obj.url = uploadTask.snapshot.metadata.downloadURLs[0];
                        ContactService.addDoc(obj);
                    });

                });

            }

            vm.removeDoc = function(id) {
                obj = {};
                obj.contact_id = vm.contact_id;
                obj.doc_id = id;
                ContactService.removeDoc(obj);
            }

        }

})();
