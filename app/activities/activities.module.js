(function() {
    'use strict';

    angular.module('app.activities', [
        'ui.grid',
        'ui.grid.edit',
        'ui.grid.cellNav',
        'ui.grid.selection'
    ])

})();
