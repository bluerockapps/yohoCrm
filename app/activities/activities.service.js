(function() {
    'use strict';

    angular
        .module('app.activities')
        .factory('ActivitiesService', ActivitiesService)
        .factory('ActivityService', ActivityService)

    ActivitiesService.$inject = ['$firebaseObject', 'DataService'];

    function ActivitiesService($firebaseObject, DataService) {

          var service = {
              activities: activities
          };

          return service;

          function activities() {
              return DataService.activities;
          }

    }

    ActivityService.$inject = ['$firebaseArray', '$firebaseObject', 'DataService'];

    function ActivityService($firebaseArray, $firebaseObject, DataService) {

          var service = {
              activityTypes: activityTypes,
              contacts: contacts,
              activity: activity,
              addActivity: addActivity,
              updateActivity: updateActivity,
              updateBusinessAnswer: updateBusinessAnswer,
              updateSolutionAnswer: updateSolutionAnswer,
              updateDiscovery: updateDiscovery,
              activityDocs: activityDocs
          };

          return service;

          function activityTypes() {
              return DataService.sales_activities;
          }

          function contacts() {
              return DataService.contacts;
          }

          function activity(id) {
              return $firebaseObject(DataService.root.ref('activities/'+ id));
          }

          function addActivity(obj) {
              obj.info_date_updated = firebase.database.ServerValue.TIMESTAMP;
              obj.info_date_added = firebase.database.ServerValue.TIMESTAMP;
              return DataService.activities.$add(obj).then(function(res){
                  return res.key;
              });
          }

          function updateActivity(obj) {
              obj.info_date_updated = firebase.database.ServerValue.TIMESTAMP;
              DataService.root.ref('activities/'+ obj.id + '/').update({info_date_updated: obj.info_date_updated, contact_id: obj.contact_id,
                  contact_full_name: obj.contact_full_name, activity_type_id: obj.activity_type_id});
          }

          function loadActivity(id) {
              return $firebaseObject(firebase.database().ref('activities/'+ id));
          }

          function updateBusinessAnswer(obj) {
              DataService.root.ref('contacts/'+ obj.id + '/business_answers/'+ obj.name).update({answer: obj.answer, date_updated: obj.date,
                time_updated: obj.time});
          }

          function updateSolutionAnswer(obj) {
              DataService.root.ref('contacts/'+ obj.id + '/solution_answers/'+ obj.name).update({answer: obj.answer, date_updated: obj.date,
                  time_updated: obj.time});
          }

          function updateDiscovery(obj) {
              if (obj.current_status === 'info') {
                  obj.discovery_date_updated = firebase.database.ServerValue.TIMESTAMP;
                  obj.discovery_date_added = firebase.database.ServerValue.TIMESTAMP;
                  DataService.root.ref('activities/'+ obj.id + '/').update({discovery_date_added: obj.discovery_date_added,
                      discovery_date_updated: obj.discovery_date_updated, current_status: 'discovery', discovery_status: true});
              } else {
                  obj.discovery_date_updated = firebase.database.ServerValue.TIMESTAMP;
                  DataService.root.ref('activities/'+ obj.id + '/').update({discovery_date_updated: obj.discovery_date_updated});
              }
          }

          function activityDocs(obj) {
              console.log(obj)
              return $firebaseArray(DataService.root.ref('contacts/'+ obj.contact_id + '/docs/').orderByChild('activity_id').equalTo(obj.activity_id));
          }
    }

})();
